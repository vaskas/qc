#!/bin/sh
# REQUIRES: unison, qc

set -e

card_dir="$(readlink -f "$1")"
dest_dir="$(readlink -f "$2")"

if [ "$dest_dir" = "" ]; then
  echo "Error: Please specify the dir"
  exit 1
fi

qc export web/plucker "$dest_dir"/PALM/Launcher 10
qc export video/audio "$dest_dir"/Listen 5

unison -fat "$dest_dir" "$card_dir"

dest_space="$(du -h "$dest_dir" | tail -1 | awk '{ print $1 }')"
echo "Space used in $dest_dir: $dest_space"

card_space="$(du -h "$card_dir" | tail -1 | awk '{ print $1 }')"
echo "Space used in $card_dir: $card_space"

