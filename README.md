# qc

Media content archival pipeline

## Problem

> So much content, so little focus

It started with dozens of open tabs, paused videos and a queue of podcasts I kept forgetting about. Books I wanted to read but never got to because smaller, more engaging dopamine-promising things kept popping up. And then, attempts to fix it all with trying to "sync" this mess with literally all my "modern" mobile device. The result was an endless spiral of recurring distractions with hardly anything read or learned.

I'm sure many people have the same sort of issue (or attitude). Maybe. Anyway, here is my solution.

## Solution

> "Media content is like sex, it's better when it's offline".

What qc can do is:

- Download and convert things! 
- If the download or conversion fails, retry
- Keep record of history in a spreadsheet to be able to pat yourself on the back
- Export to external locations / media, keeping track what you copied and when.

### Conversion types

- Web URLs to Plucker, Epub, txt and spoken mp3 audio
- Video web URLs and local files to mp3 audio with videos being archived too
- Ebooks to txt, PDB and Plucker

Note the choice of formats (haha). QC might work for you if you like command line tools, or use specialised or "retro" devices for different media types:

- Yes, an iPhone is powerful enough for plain text but so is a Palm Pilot from 1997! Or an HP DOS handheld from 1993. And the latter come with less distractions, no ads and good battery life. 

- Need to play some videos? A 2013 Blackberry phone handles full HD H.264 just fine. Also supports the microHDMI cable so no WLAN magic is needed to play them on the big screen.

You got the idea. Reusing stuff. Take that, Big Tech.

### Installation

### Usage


Add a new item:

```
qc add web <url>
qc add video <url>
qc add clipboard
qc add epub <path to file>
```

Retry stuck items:

```
qc retry
```

View queue states:

```
qc status
```

Export from collection:

```
qc export <media type> <destination directory> <number of items (default=all)>
```

Where `<media type>` options are:

```
audio
books/epub
books/fb2
books/pdf
books/plucker
papers/pdf
video
video/audio
web/audio
web/epub
web/plucker
```

See `$HOME/.config/qc/config` for configuration options.

